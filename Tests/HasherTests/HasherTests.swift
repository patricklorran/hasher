import XCTest
@testable import Hasher

final class HasherTests: XCTestCase {
    
    let string = "Hash me if you can."
    
    func testHashStringWithMD2() {
        guard let data = string.data(using: .utf8) else {
            XCTFail("Unable to convert string into Data.")
            return
        }
        let result = Hasher.hash(data, algorithm: .md2)
        XCTAssertEqual(result, "463e5475aa844e73672f92b6eb32aa78")
    }
    
    func testHashStringWithMD4() {
        guard let data = string.data(using: .utf8) else {
            XCTFail("Unable to convert string into Data.")
            return
        }
        let result = Hasher.hash(data, algorithm: .md4)
        XCTAssertEqual(result, "e8acca27d2bde34a10547bfe686d9ea2")
    }
    
    func testHashStringWithMD5() {
        guard let data = string.data(using: .utf8) else {
            XCTFail("Unable to convert string into Data.")
            return
        }
        let result = Hasher.hash(data, algorithm: .md5)
        XCTAssertEqual(result, "2dbb1c157f237ad74f72a50a0d5a1f57")
    }
    
    func testHashStringWithSHA1() {
        guard let data = string.data(using: .utf8) else {
            XCTFail("Unable to convert string into Data.")
            return
        }
        let result = Hasher.hash(data, algorithm: .sha1)
        XCTAssertEqual(result, "e4ff8a2b25b8074c13557c943646c6fa9c2ec3f7")
    }
    
    func testHashStringWithSHA256() {
        guard let data = string.data(using: .utf8) else {
            XCTFail("Unable to convert string into Data.")
            return
        }
        let result = Hasher.hash(data, algorithm: .sha256)
        XCTAssertEqual(result, "d09d946d5655fc84c048769dca6eb575eda06676e66952b4d13e4e37c25b12e9")
    }
    
    func testHashStringWithSHA384() {
        guard let data = string.data(using: .utf8) else {
            XCTFail("Unable to convert string into Data.")
            return
        }
        let result = Hasher.hash(data, algorithm: .sha384)
        XCTAssertEqual(result, "d28b663d52a98a444a77107dc9ba64ec5bdd62cf082442cf5a641faeabe3dec34a1b8f9f624f12d503ed999cf2cc26a5")
    }
    
    func testHashStringWithSHA512() {
        guard let data = string.data(using: .utf8) else {
            XCTFail("Unable to convert string into Data.")
            return
        }
        let result = Hasher.hash(data, algorithm: .sha512)
        XCTAssertEqual(result, "9cbd8c9deaa84bc3f8bee807f84d99f45d9857c15c72cbe5e457b7bba7762ba11fda48c5abe3ac2a796d951c39bc49c7066734216abe9e51b233fe0ed00aa3b5")
    }
    
    static var allTests = [
        ("testHashStringWithMD2", testHashStringWithMD2),
        ("testHashStringWithMD4", testHashStringWithMD4),
        ("testHashStringWithMD5", testHashStringWithMD5),
        ("testHashStringWithSHA1", testHashStringWithSHA1),
        ("testHashStringWithSHA256", testHashStringWithSHA256),
        ("testHashStringWithSHA384", testHashStringWithSHA384),
        ("testHashStringWithSHA512", testHashStringWithSHA512)
    ]
}
