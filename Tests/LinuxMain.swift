import XCTest

import HashTests

var tests = [XCTestCaseEntry]()
tests += HashTests.allTests()
XCTMain(tests)
