# Hasher

Hasher is a simple hash calculator that wraps the hahsing functions available in Apple's Common Crypto module. It performs computations using Message Digest (MD) and Secure Hash Algorithms (SHA) algorithms. Please note that MD algorithms are insecure and are intended to use for compatibility purposes.

## Installation

Hasher uses the new Swift Package Manager. To know more about the project, please refer to its [official website](https://swift.org/package-manager/).

To add Hasher to your project, add the following package URL under Xcode menu File > Swift Packages > Add Package Dependency… :

    https://gitlab.com/patricklorran/hasher.git

## Usage

Hasher computes the hash of a given `Data` object, returning a `String?` object desbribing the calculation result. For example, to get the hash from a `String` do the following:

```swift
let text = "Hash me if you can."
let data = text.data(using: .utf8)
let hash = Hasher.hash(data!, algorithm: .sha256)
```

This returns a  `String?` object with the hexadecimal value `d09d946d5655fc84c048769dca6eb575eda06676e66952b4d13e4e37c25b12e9`.

Use the `algorithm` param to specify the hashing algorithm. There's also a `outputType` param where you can specify the output format, which is either hexadecimal (default) or Base64.

Remember that MD algorithms are insecure and should be used for compatibility purposes.

## Author information

Developed by [Patrick Lorran](https://patricklorran.com)

If you find an issue feel free to contact me.
