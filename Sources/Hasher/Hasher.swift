/*
 * MIT License
 *
 * Copyright (c) 2019 Patrick Lorran
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import CommonCrypto
import Foundation

/// A hash calculator.
///
/// Hasher computes the hash from `Data` objects. You can specify the algorithm and the type of output returned from the calculation.
///
/// For example, the following implementation demonstrates how to compute a MD5 hash from a given string:
///
///     let text = "Hash me if you can."
///     let data = text.data(using: .utf8)
///     let hash = Hasher.hash(data!, algorithm: .sha256)
///
/// This produces the optional string `"d09d946d5655fc84c048769dca6eb575eda06676e66952b4d13e4e37c25b12e9"`. If no output format is specified, as shown above, the result string will be in hex format.
public struct Hasher {
    
    private init() {}
    
    // MARK: - Hash Algorithms
    
    /// Specifies a hash algorithm.
    public enum HashAlgorithm {
        
        // MARK: Legacy Algorithms
        
        /// An implementation of MD2 hashing.
        /// - Warning: This algorithm is insecure and is intended to use for backward compatibility with older services that requires it.
        case md2
        
        /// An implementation of MD4 hashing.
        /// - Warning: This algorithm is insecure and is intended to use for backward compatibility with older services that requires it.
        case md4
        
        /// An implementation of MD5 hashing.
        /// - Warning: This algorithm is insecure and is intended to use for backward compatibility with older services that requires it.
        case md5
        
        /// An implementation of SHA1 hashing.
        /// - Warning: This algorithm is insecure and is intended to use for backward compatibility with older services that requires it.
        case sha1
        
        // MARK: Secure Algorithms
        
        /// An implementation of SHA224 hashing.
        case sha224
        
        /// An implementation of SHA256 hashing.
        case sha256
        
        /// An implementation of SHA384 hashing.
        case sha384
        
        /// An implementation of SHA512 hashing.
        case sha512
        
        // MARK: Algorithm's Digest Lenght
        
        /// Returns the digest length of the given algorithm.
        var length: Int32 {
            switch self {
                case .md2:      return CC_MD2_DIGEST_LENGTH
                case .md4:      return CC_MD4_DIGEST_LENGTH
                case .md5:      return CC_MD5_DIGEST_LENGTH
                case .sha1:     return CC_SHA1_DIGEST_LENGTH
                case .sha224:   return CC_SHA224_DIGEST_LENGTH
                case .sha256:   return CC_SHA256_DIGEST_LENGTH
                case .sha384:   return CC_SHA384_DIGEST_LENGTH
                case .sha512:   return CC_SHA512_DIGEST_LENGTH
            }
        }
    }
    
    // MARK: - Hashing an Object
    
    /// Specifies the output type of a hash calculation.
    public enum HashOutputType {
        /// Outputs data in hex format.
        case hex
        /// Outputs data in base 64 format.
        case base64
    }
    
    /// Calculates the hash of a given data.
    /// - Parameter string: The data on which the calculation shall be performed.
    /// - Parameter algorithm: The hash algorithm used for the calculation.
    /// - Parameter outputType: The type of calculated hash. Default is `.hex`.
    /// - Returns: The resulting hash, or `nil` if the computation fails.
    public static func hash(_ data: Data, algorithm: HashAlgorithm, outputType: HashOutputType = .hex) -> String? {
        return _hash(data, algorithm: algorithm, outputType: outputType)
    }
    
    /// Calculates the hash of a given data prepending a RSA 2048 ASN 1 header to its beginning.
    /// - Parameter data: The data on which the calculation shall be performed.
    /// - Parameter algorithm: The hash algorithm used for the calculation.
    /// - Parameter outputType: The type of calculated hash. Default is `.hex`.
    /// - Returns: The resulting hash, or `nil` if the computation fails.
    public static func hashWithRSA2048ASN1Header(_ data: Data, algorithm: HashAlgorithm, outputType: HashOutputType = .hex) -> String? {
        let RSA2048ASN1Header:[UInt8] = [
            0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86,
            0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00
        ]
        
        var headerData = Data(RSA2048ASN1Header)
        headerData.append(data)
        
        return _hash(headerData, algorithm: algorithm, outputType: outputType)
    }
}
