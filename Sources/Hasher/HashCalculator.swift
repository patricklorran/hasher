/*
 * MIT License
 *
 * Copyright (c) 2019 Patrick Lorran
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import Foundation
import CommonCrypto

func _hash(_ data: Data, algorithm: Hasher.HashAlgorithm, outputType: Hasher.HashOutputType) -> String? {
    
    var digest = [UInt8](repeating: 0, count: Int(algorithm.length))
    let length = CC_LONG(data.count)
    
    data.withUnsafeBytes { pointer in
        let buffer = pointer.baseAddress
        
        switch algorithm {
            case .md2: CC_MD2(buffer, length, &digest)
            case .md4: CC_MD4(buffer, length, &digest)
            case .md5: CC_MD5(buffer, length, &digest)
            case .sha1: CC_SHA1(buffer, length, &digest)
            case .sha224: CC_SHA224(buffer, length, &digest)
            case .sha256: CC_SHA256(buffer, length, &digest)
            case .sha384: CC_SHA384(buffer, length, &digest)
            case .sha512: CC_SHA512(buffer, length, &digest)
        }
    }
    
    switch outputType {
        case .hex:      return digest.map { String(format: "%02hhx", $0) }.joined()
        case .base64:   return Data(digest).base64EncodedString()
    }
}
